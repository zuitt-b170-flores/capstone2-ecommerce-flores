const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");

// access to routes
const userRoutes = require("./routes/userRoutes.js")
const productRoutes = require("./routes/productRoutes.js")
const orderRoutes = require("./routes/orderRoutes.js")
const reviewRoutes = require("./routes/reviewRoutes")

// server
const app = express();
const port = 4000;

app.use(cors())//allows all origins/domains to access the backend application

app.use(express.json())
app.use(express.urlencoded( { extended:true } ))



mongoose.connect("mongodb+srv://DenzelFlores:Jazmyne08@wdc028-course-booking.oevj9.mongodb.net/capstone2-ecommerce?retryWrites=true&w=majority", {
	useNewUrlParser: true,
	useUnifiedTopology:true
	})

let db = mongoose.connection;
db.on("error", console.error.bind(console, "Connection Error"));
db.once("open", () => console.log("We're connected to the database"))

app.use("/api/users", userRoutes);
app.use("/api/products", productRoutes);
app.use("/api/orders", orderRoutes);
app.use("/api/reviews", reviewRoutes)


app.listen(process.env.PORT || port, () => console.log(`API now online at port ${process.env.PORT || port}`))
// process.env.PORT works if you are deploying the api in a host like Heroku. this code allows the app to use the environment of that host (Heroku) in running the server