const mongoose = require("mongoose")
const reviewSchema = new mongoose.Schema({
	productId:{
		type:String,
		required: [true, "productId is required"]
	},
	name:{
		type:String,
		required: [true, "name is required"]
	},
	review:{
		type:String,
		required: [true, "review is required"]
	}
})
module.exports=mongoose.model('Review', reviewSchema)
