const mongoose = require("mongoose")
const userSchema = new mongoose.Schema(
  {
    userName:{
    type:String,
    required:[true,"userName is required"]
  },
    email:{
    type:String,
    required:[true,"email is required"]
  },
    password:{
    type:String,
    required:[true,"password is required"]
  },
    isAdmin:{
    type:Boolean,
    default:false
  },
    wishlist:[
    {
      productId: {
        type:String,
        required:[true, "review is required"]
        },
      name:{
        type:String,
        required: [true, "name is required"]
      }
    }
  ],
    orders:[
    {
      productId: {
        type:String,
        required:[true, "orderId is required"]
        }
    },
    {
      price: {
          type:Number,
          required:[true,"price is required"]
        }
    },
    {
      quantity: {
          type:Number,
          default:1
      }
    },
    {
      status: {
          type:String,
          default: "Waiting for payment"
      }
    },
    {
      totalAmount:{
        type:Number,
        required:[true,"totalAmount is required"]
      }
    }
  ]
})
module.exports=mongoose.model('User', userSchema)

