const express = require("express");
const router = express.Router();

const auth = require("../auth")
const userController = require("../controllers/userControllers")

//Route for checking if the user's email already exists in the database
router.post('/checkEmail', (req, res)=>{
	userController.checkEmailExists(req.body).then(result => res.send(result))
})

// Get all users (admin only)
router.get("/allUsers", auth.verify, (req,res)=>{
	const userData = auth.decode(req.headers.authorization)
	userController.getUsers({userId:userData.id, isAdmin:userData.isAdmin}).then(result =>res.send(result))
})
// User Registration
router.post("/register",(req,res)=>{
	userController.registerUser(req.body).then(result=>res.send(result));
})

// Route for retrieving user details
router.get("/details", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization);
	// Provides the user's ID for the getProfile controller method
	userController.getProfile({userId : userData.id}).then(resultFromController => res.send(resultFromController));
});


// User Login/Authentication
router.post('/login',(req,res)=>{
	userController.loginUser(req.body).then(result => res.send(result));
})

// Set user as admin
router.put("/setAsAdmin/:id", auth.verify, (req,res)=>{
	const userData = auth.decode(req.headers.authorization)
	userController.setAsAdmin(req.body, {userId: userData.id, isAdmin:userData.isAdmin}).then(result=>res.send(result));
})


// Adding a Product to a User's Wishlist
router.post('/addToWishlist', auth.verify, (req, res) =>{
	let data = {
		productId: req.body.productId,
		name: req.body.name
	}
	const userData = auth.decode(req.headers.authorization)
	userController.addToWishlist(data, {userId : userData.id}).then(result => res.send(result))
})

// getting wishlist of a user
router.get("/wishlist/:userId", auth.verify, (req,res)=>{
	const userData = auth.decode(req.headers.authorization)
	userController.getWishlist(req.params, {userId: userData.id}).then(result =>res.send(result))
})


// Adding a Product to a User's Orders
router.post('/createOrder', auth.verify, (req, res) =>{
	let data = {
		totalAmount: `${req.body.quantity * req.body.price}`,
		productId: req.body.productId,
		price: req.body.price,
		quantity: req.body.quantity
}
	const userData = auth.decode(req.headers.authorization);
	userController.createOrder(data, {userId:userData.id, orders:userData.orders}).then(result => res.send(result))
})

// getting the orders of a user
router.get("/:userId/orders", auth.verify, (req,res)=>{
	const userData = auth.decode(req.headers.authorization)
	userController.getOrders(req.params.userId, {userId: userData.id}).then(result =>res.send(result))
})



module.exports=router;


// express mongoose bcrypt cors jsonwebtoken
