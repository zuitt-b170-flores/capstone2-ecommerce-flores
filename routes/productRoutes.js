const express = require("express");
const router = express.Router();

const auth = require("../auth")
const productController = require("../controllers/productControllers")

// Retrieve Active Products
router.get("/active", (req,res)=>{
	productController.getActiveProducts().then(result =>res.send(result))
})

// Retrieve All Products
router.get("/all", (req,res)=>{
	productController.getAllProducts().then(result =>res.send(result))
})

//Retrieve a Product
router.get("/:productId", (req,res)=>{
	console.log("ID: ", req.params.productId)
	productController.getProduct(req.params).then(result =>res.send(result))
})

// Create a Product
router.post("/create", auth.verify, (req, res) => {
    const userData = auth.decode(req.headers.authorization)
    productController.addProduct(req.body, {userId: userData.id, isAdmin:userData.isAdmin}).then(result => res.send(result))
})


// Update a Product
router.put("/:productId/update", auth.verify, (req,res)=>{
	const userData = auth.decode(req.headers.authorization)
	productController.updateProduct(req.body, {userId: userData.id, isAdmin:userData.isAdmin}).then(result =>res.send(result))
})

// archiving a product
router.put("/:productId/archive", auth.verify, (req,res)=>{
	const userData = auth.decode(req.headers.authorization)
	productController.archiveProduct(req.params, {userId: userData.id, isAdmin:userData.isAdmin}).then(result =>res.send(result))
})

// unarchiving a product
router.put("/:productId/unarchive", auth.verify, (req,res)=>{
	const userData = auth.decode(req.headers.authorization)
	productController.unarchiveProduct(req.params, {userId: userData.id, isAdmin:userData.isAdmin}).then(result =>res.send(result))
})


module.exports=router;
