const express = require("express");
const router = express.Router();
const auth = require("../auth")
const orderController = require("../controllers/orderControllers")



// Get all orders
router.get("/allorders", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization);
    orderController.getAllOrders({userId: userData.id, isAdmin:userData.isAdmin}).then(result => res.send(result))
})

// create an Order
router.post('/checkout', auth.verify, (req, res, ) =>{
	const userData = auth.decode(req.headers.authorization);
	orderController.createOrder(req.body, {userId:userData.id, purchase:userData.purchase}).then(result => res.send(result))
})


// Get an order
router.get("/:id/orders", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization);
    orderController.getUserOrder(req.body, {userId: userData.id}).then(result => res.send(result))
})

 
// Cancel an Order
router.put("/:orderId/cancel", auth.verify, (req,res)=>{
	const userData = auth.decode(req.headers.authorization)
	orderController.cancelOrder(req.params, {userId: userData.id}).then(result =>res.send(result))
})

// Update a Product
router.put("/:orderId/update", auth.verify, (req,res)=>{
	const userData = auth.decode(req.headers.authorization)
	orderController.updateStatus(req.body, {userId: userData.id, isAdmin:userData.isAdmin}).then(result =>res.send(result))
})


module.exports=router;
