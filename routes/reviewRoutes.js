const express = require("express");
const router = express.Router();

const auth = require("../auth")
const reviewController = require("../controllers/reviewControllers")

// Retrieve Reviews
router.get("/", (req,res)=>{
	reviewController.getReviews(req.params).then(result =>res.send(result))
})

// Create Review
router.post("/:productId/create", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization)
	let data = ({
	    productId: req.params.productId,
	    name: req.body.name,
	    review: req.body.review
	})
    reviewController.createReview(data, {userId: userData.id}).then(result => res.send(result))
})


module.exports=router;
