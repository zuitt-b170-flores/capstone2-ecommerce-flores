const User = require("../models/User")
const Product = require("../models/Product")
const Order = require("../models/Order")
const Review = require("../models/Review")
const bcrypt = require('bcrypt');
const auth = require("../auth")



// Retrieve Reviews
module.exports.getReviews=(reqParams)=>{
		return Product.findById(reqParams.productId).then(result=>{
			console.log(reqParams)
			return result.reviews
	})
}


module.exports.createReview=async(data, userData)=>{
	let isReviewUpdated = await Product.findById(data.productId).then(product => {
		return User.findById(userData.userId).then(result => {
		    if (result==null) {
		    	return "You must login first"
		    }else{
			    return Product.findById(data.productId).then(result=>{
			    	if (result==null) {
			    		return "Product does not exist"
			    	}else{
			    		return Order.findById(data.orderId).then(result=>{
			    			if (result == null) {
			    				return "You must order first"
			    			}else{
									product.reviews.push({review:data.review})
									return product.save().then((product, err)=>{
										if (err) {
											return false
										}else{
											return true
										}
									})

							}
						})
					}
				})
			}
		})
	})

	if (isReviewUpdated){
		return true
	}else{
		return false
	}
}
